<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\controller\interfaces;

/**
 * Интерфейс IController объявляет методы контроллера.
 */
interface ControllerComponentInterface
{
    public const ACTION_PREFIX = 'action';

    /**
     * Метод  исполнения действия.
     *
     * @param string $route Роут.
     *
     * @return void
     */
    public function runAction(string $route): void;
}
