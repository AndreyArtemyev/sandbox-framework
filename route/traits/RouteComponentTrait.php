<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\route\traits;

use Exception;
use SandBox\SandBoxFramework\base\Base;
use SandBox\SandBoxFramework\route\interfaces\RouteComponentInterface;

/**
 * Трэит WithRouteComponent подключает переменную компонента к классу.
 */
trait RouteComponentTrait
{
    /**
     * Свойство содержит компонент.
     *
     * @var RouteComponentInterface|null
     */
    protected $routeComponent;

    /**
     * Метод возвращает компонент.
     *
     * @throws Exception Если компонент не зарегистирован.
     *@return RouteComponentInterface
     *
     */
    protected function getRouteComponent(): RouteComponentInterface
    {
        if (null === $this->routeComponent) {
            $this->routeComponent = Base::getApplication()->getComponent(RouteComponentInterface::COMPONENT_NAME);
        }

        return $this->routeComponent;
    }

    /**
     * Метод задает компонент.
     *
     * @param RouteComponentInterface $value Новое значение.
     *
     * @return void
     */
    protected function setRouteComponent(RouteComponentInterface $value): void
    {
        $this->routeComponent = $value;
    }
}
