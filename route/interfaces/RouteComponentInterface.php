<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\route\interfaces;

use SandBox\SandBoxFramework\application\interfaces\ApplicationComponentInterface;

/**
 * Интерфейс Route объявляет методы поиска контроллера.
 */
interface RouteComponentInterface extends ApplicationComponentInterface
{
    public const COMPONENT_NAME = 'route';

    /**
     * Метод возвращает объект контроллера.
     *
     * @param string $route роут контроллера.
     *
     * @return RouteComponentInterface|null
     */
    public function findController(string $route): ?RouteComponentInterface;
}
