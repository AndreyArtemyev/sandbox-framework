<?php

namespace SandBox\SandBoxFramework\request\parsers;


use SandBox\SandBoxFramework\base\BaseObject;
use SandBox\SandBoxFramework\request\interfaces\parsers\ParserInterface;

/**
 * Класс JsonParser реализует методы парса json запросов.
 */
class JsonParser extends BaseObject implements ParserInterface
{
    protected const REQUEST_BODY_STREAM = 'php://input';

    /**
     * Метод парсит входящий ответ.
     *
     * @return array
     */
    public function parse(): array
    {
        return json_decode(file_get_contents(static::REQUEST_BODY_STREAM), true);
    }
}
