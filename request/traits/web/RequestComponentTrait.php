<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\request\traits\web;

use Exception;
use SandBox\SandBoxFramework\base\Base;
use SandBox\SandBoxFramework\request\interfaces\components\web\RequestComponentInterface;

/**
 * Трэит WithRequestComponent подключает переменную компонента к классу.
 */
trait RequestComponentTrait
{
    /**
     * Свойство содержит компонент.
     *
     * @var RequestComponentInterface|null
     */
    protected $requestComponent;

    /**
     * Метод возвращает компонент.
     *
     * @throws Exception Если компонент не зарегистирован.
     *@return RequestComponentInterface
     *
     */
    protected function getRequestComponent(): RequestComponentInterface
    {
        if (null === $this->requestComponent) {
            $this->requestComponent = Base::getApplication()->getComponent(RequestComponentInterface::COMPONENT_NAME);
        }

        return $this->requestComponent;
    }

    /**
     * Метод задает компонент.
     *
     * @param RequestComponentInterface $value Новое значение.
     *
     * @return void
     */
    protected function setRequestComponent(RequestComponentInterface $value): void
    {
        $this->requestComponent = $value;
    }
}
