<?php

namespace SandBox\SandBoxFramework\request\interfaces\parsers;

/**
 * Интерфейс IParser объявляет методы парсера.
 */
interface ParserInterface
{
    /**
     * Метод парсит входящий ответ.
     *
     * @return array
     */
    public function parse(): array;
}
