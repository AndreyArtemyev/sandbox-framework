<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\request\interfaces\components\web;

use SandBox\SandBoxFramework\application\interfaces\ApplicationComponentInterface;

/**
 * Интерфейс IRequest объявляет методы компонента.
 */
interface RequestComponentInterface extends ApplicationComponentInterface
{
    public const COMPONENT_NAME = 'request';

    /**
     * Метод возвращает путь обращения.
     *
     * @return string
     */
    public function getRouteName(): string;

    /**
     * Метод возвращает параметры запроса метода GET.
     *
     * @param string $key Ключ для выборки.
     *
     * @return string|null
     */
    public function getByKey(string $key): ?string;

    /**
     * Метод возвращает параметры запроса метода POST.
     *
     * @return array
     */
    public function post(): array;
}
