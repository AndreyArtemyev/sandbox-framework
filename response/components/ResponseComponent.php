<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\response\components;

use SandBox\SandBoxFramework\base\BaseObject;
use SandBox\SandBoxFramework\response\interfaces\ResponseComponentInterface;

/**
 * Класс-компонент Response реализует методы отправки ответа.
 */
class ResponseComponent extends BaseObject implements ResponseComponentInterface
{
    /**
     * Свойство содержит список заголовков.
     *
     * @var array
     */
    protected $headerList = [];

    /**
     * Метод задает список заголовков.
     *
     * @param array $value
     *
     * @return ResponseComponentInterface
     */
    public function setHeaderList(array $value): ResponseComponentInterface
    {
        $this->headerList = $value;

        return $this;
    }

    /**
     * Метод возвращает список заголовков.
     *
     * @return array
     */
    public function getHeaderList(): array
    {
        return (array)$this->headerList;
    }

    /**
     * Метод добавляет/заменяет заголовок к ответу.
     *
     * @param string $key   Название заголовка.
     * @param string $value Значение.
     *
     * @return ResponseComponentInterface
     */
    public function addHeader(string $key, string $value): ResponseComponentInterface
    {
        $this->headerList[$key] = $value;

        return $this;
    }

    /**
     * Метод отправляет ответ пользователю.
     *
     * @param string $content Сообщение для пользователя.
     * @param int    $code    Код ответа.
     *
     * @return void
     */
    public function send(string $content, int $code = 200): void
    {
        $this->sendHeaders($code);
        $this->sendContent($content);
    }

    /**
     * Метод формирует заголовки для сообщения.
     *
     * @param int $code Код ответа.
     *
     * @return void
     */
    protected function sendHeaders(int $code): void
    {
        header('HTTP/1.1 ' . $code);

        foreach ($this->getHeaderList() as $name => $value) {
            header(sprintf('%s: %s', $name, $value));
        }
    }

    /**
     * Метод отправляет контент.
     *
     * @param string $content Сообщение для пользователя.
     *
     * @return void
     */
    protected function sendContent(string $content): void
    {
        echo $content;
    }
}
