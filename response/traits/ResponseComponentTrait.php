<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\response\traits;

use Exception;
use SandBox\SandBoxFramework\base\Base;
use SandBox\SandBoxFramework\response\interfaces\ResponseComponentInterface;

/**
 * Трэит WithResponseComponent подключает переменную компонента к классу.
 */
trait ResponseComponentTrait
{
    /**
     * Свойство содержит компонент.
     *
     * @var ResponseComponentInterface|null
     */
    protected $responseComponent;

    /**
     * Метод возвращает компонент.
     *
     * @throws Exception Если компонент не зарегистирован.
     *@return ResponseComponentInterface
     *
     */
    protected function getResponseComponent(): ResponseComponentInterface
    {
        if (null === $this->responseComponent) {
            $this->responseComponent = Base::getApplication()->getComponent(ResponseComponentInterface::COMPONENT_NAME);
        }

        return $this->responseComponent;
    }

    /**
     * Метод задает компонент.
     *
     * @param ResponseComponentInterface $value Новое значение.
     *
     * @return void
     */
    protected function setResponseComponent(ResponseComponentInterface $value): void
    {
        $this->responseComponent = $value;
    }
}
