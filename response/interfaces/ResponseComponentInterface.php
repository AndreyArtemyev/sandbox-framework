<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\response\interfaces;

use SandBox\SandBoxFramework\application\interfaces\ApplicationComponentInterface;

/**
 * Интерфейс IRequest объявляет методы компонента.
 */
interface ResponseComponentInterface extends ApplicationComponentInterface
{
    public const COMPONENT_NAME = 'response';

    /**
     * Метод задает список заголовков.
     *
     * @param array $value
     *
     * @return ResponseComponentInterface
     */
    public function setHeaderList(array $value): ResponseComponentInterface;

    /**
     * Метод возвращает список заголовков.
     *
     * @return array
     */
    public function getHeaderList(): array;

    /**
     * Метод добавляет/заменяет заголовок к ответу.
     *
     * @param string $key   Название заголовка.
     * @param string $value Значение.
     *
     * @return ResponseComponentInterface
     */
    public function addHeader(string $key, string $value): ResponseComponentInterface;

    /**
     * Метод отправляет ответ пользователю.
     *
     * @param string $content Сообщение для пользователя.
     * @param int    $code    Код ответа.
     *
     * @return void
     */
    public function send(string $content, int $code = 200): void;
}
