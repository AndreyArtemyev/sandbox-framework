<?php

namespace SandBox\SandBoxFramework\form;


use SandBox\SandBoxFramework\base\BaseObject;
use SandBox\SandBoxFramework\interfaces\FormInterface;
use SandBox\SandBoxFramework\result\interfaces\BaseResultInterface;
use SandBox\SandBoxFramework\result\traits\BaseResultObjectTrait;

/**
 * Класс BaseForm реализует методы базовой формы.
 */
abstract class BaseForm extends BaseObject implements FormInterface
{
    use BaseResultObjectTrait;

    /**
     * Метод реализует загрузку формы.
     *
     * @param array $data Входящие данные.
     *
     * @return bool
     */
    public function load(array $data): bool
    {
        foreach ($data as $attributeName => $value) {
            if (property_exists($this, $attributeName)) {
                $this->$attributeName = $value;
            }
        }

        return true;
    }

    /**
     * Метод реализует основное действие формы.
     *
     * @return BaseResultInterface
     */
    abstract public function run(): BaseResultInterface;
}