<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\interfaces;

use SandBox\SandBoxFramework\result\interfaces\BaseResultInterface;
use SandBox\SandBoxFramework\result\interfaces\BaseResultObjectInterface;

/**
 * Интерфейс IForm объявляет методы базовой формы.
 */
interface FormInterface extends BaseResultObjectInterface
{
    /**
     * Метод реализует загрузку формы.
     *
     * @param array $data Входящие данные.
     *
     * @return bool
     */
    public function load(array $data): bool;

    /**
     * Метод реализует основное действие формы.
     *
     * @return BaseResultInterface
     */
    public function run(): BaseResultInterface;
}
