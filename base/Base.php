<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\base;

use Exception;
use ReflectionClass;
use ReflectionException;
use SandBox\SandBoxFramework\application\interfaces\web\ApplicationInterface;

class Base
{
    /**
     * Свойство хранит объект приложения.
     *
     * @var ApplicationInterface|null
     */
    protected static $application;

    /**
     * Метод задает объект приложения.
     *
     * @param ApplicationInterface $value Новое значение.
     *
     * @return void
     */
    public static function setApplication($value): void
    {
        static::$application = $value;
    }

    /**
     * Метод возвращает объект приложения.
     *
     * @return ApplicationInterface
     *
     * @throws Exception Если объект приложения не задан.
     */
    public static function getApplication()
    {
        if (null === static::$application) {
            throw new Exception('Приложение не может быть пустым.');
        }

        return static::$application;
    }

    /**
     * Метод инициализации приложения.
     *
     * @param array $config Конфигурация приложения.
     *
     * @throws ReflectionException
     */
    public static function init($config = [])
    {
        static::$application = static::createObject($config);
    }

    /**
     * Метод создания объекта (Почти как DI).
     *
     * @param array $config Конфигурация объекта.
     *
     * @return object
     *
     * @throws ReflectionException Если рефлекшены в плохом настроении.
     * @throws Exception Если отсутствует название класса в конфигурации.
     */
    public static function createObject($config = [])
    {
        $argumentList = [];

        $class = $config['class'] ?? null;
        unset($config['class']);
        if (! $class) {
            throw new Exception('Название класса отсутствует.');
        }

        foreach ($config as $attribute => $value) {
            $dependencyClass = $value['class'] ?? null;

            if ($dependencyClass) {
                $value = static::createObject($value);
            }

            $argumentList[$attribute] = $value;
        }

        $reflection = new ReflectionClass($class);
        if ($reflection->implementsInterface('SandBox\SandBoxFramework\base\BaseObjectInterface')) {
            return $reflection->newInstance($argumentList);
        }

        return $reflection->newInstanceArgs($argumentList);
    }
}