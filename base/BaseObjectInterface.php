<?php
declare(strict_types = 1);

namespace SandBox\SandBoxFramework\base;

interface BaseObjectInterface
{
    /**
     * Базой конструктор.
     *
     * @param array $config Конфигурация для конструктора.
     *
     * @return void
     */
    public function __construct(array $config = []);
}