<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\application\interfaces;

/**
 * интерфейс IComponent объявляет методы любого компонента.
 */
interface ApplicationComponentInterface
{

}
