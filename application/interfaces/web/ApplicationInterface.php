<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\application\interfaces\web;

use Exception;
use SandBox\SandBoxFramework\application\interfaces\ApplicationComponentInterface;

/**
 * Интерфейс IApplication объявляет методы приложения.
 */
interface ApplicationInterface
{
    /**
     * Метод исполнения заветных желаний.
     *
     * @return void
     */
    public function run(): void;

    /**
     * Метод возвращает компонент по его названию.
     *
     * @param string $name Название компонента.
     *
     * @throws Exception
     * @return ApplicationComponentInterface
     *
     */
    public function getComponent(string $name): ApplicationComponentInterface;
}
