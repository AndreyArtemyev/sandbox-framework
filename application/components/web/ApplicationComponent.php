<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\application\components\web;

use Exception;
use SandBox\SandBoxFramework\application\components\BaseApplicationComponent;
use SandBox\SandBoxFramework\application\interfaces\web\ApplicationInterface;
use SandBox\SandBoxFramework\request\traits\web\RequestComponentTrait;
use SandBox\SandBoxFramework\route\traits\RouteComponentTrait;

/**
 * Класс приложения.
 */
class ApplicationComponent extends BaseApplicationComponent implements ApplicationInterface
{
    use RequestComponentTrait;
    use RouteComponentTrait;

    /**
     * Метод исполнения заветных желаний.
     *
     * @return void
     *
     * @throws Exception
     */
    public function run(): void
    {
        $route = $this->getRequestComponent()->getRouteName();

        $controller = $this->getRouteComponent()->findController($route);

        $controller->runAction($route);
    }
}
