<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\application\components;

use Exception;
use SandBox\SandBoxFramework\application\interfaces\ApplicationComponentInterface;
use SandBox\SandBoxFramework\base\Base;
use SandBox\SandBoxFramework\base\BaseObject;

/**
 * Класс приложения.
 */
abstract class BaseApplicationComponent extends BaseObject
{
    /**
     * Свойство содержит список компонентов.
     *
     * @var array
     */
    protected $componentList = [];

    /**
     * Метод задает список компонентов.
     *
     * @param array $value
     */
    public function setComponentList(array $value): void
    {
        $value = array_map(function($config) {
            return Base::createObject($config);
        }, $value);

        $this->componentList = $value;
    }

    /**
     * Метод возвращает компонент по его названию.
     *
     * @param string $name Название компонента.
     *
     * @return ApplicationComponentInterface
     *
     * @throws Exception
     */
    public function getComponent(string $name): ApplicationComponentInterface
    {
        $component = $this->componentList[$name] ?? null;
        if (! $component instanceof ApplicationComponentInterface) {
            throw new Exception($name . ' компонент не задан.');
        }

        return $component;
    }

    /**
     * Метод исполнения заветных желаний.
     *
     * @return void
     *
     * @throws Exception
     */
    abstract public function run(): void;
}
