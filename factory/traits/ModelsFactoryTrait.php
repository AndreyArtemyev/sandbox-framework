<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\factory\traits;

use Exception;
use SandBox\SandBoxFramework\factory\interfaces\FactoryInterface;

/**
 * Трэит WithFactoryTrait подключает фабрику к классу.
 */
trait ModelsFactoryTrait
{
    /**
     * Свойство хранит объект фабрики.
     *
     * @var FactoryInterface|null
     */
    protected $modelFactory;

    /**
     * Метод задает объект фабрики.
     *
     * @param FactoryInterface $value Новое значение.
     *
     * @return static
     */
    public function setModelFactory(FactoryInterface $value)
    {
        $this->modelFactory = $value;
        return $this;
    }

    /**
     * Метод возвращает объект фабрики.
     *
     * @throws Exception Если класс фабрики отсутствует.
     *@return FactoryInterface
     *
     */
    protected function getModelFactory(): FactoryInterface
    {
        if (null === $this->modelFactory) {
            throw new Exception('Объект фабрики отсутствует.');
        }
        return $this->modelFactory;
    }
}
