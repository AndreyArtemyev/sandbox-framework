<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\factory\interfaces;

/**
 * Интерфейс IWithFactory объявляет методы присутствия фабрики.
 */
interface ModelsFactoryTraitInterface
{
    /**
     * Метод задает объект фабрики.
     *
     * @param FactoryInterface $value Новое значение.
     *
     * @return static
     */
    public function setFactory(FactoryInterface $value);
}
