<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\factory\interfaces;

/**
 * Интерфейс IFactory объявляет методы фабрики.
 */
interface FactoryInterface
{
    /**
     * Метод задает конфигурацию для создания объектов.
     *
     * @param array $value
     *
     * @return static
     */
    public function setConfig(array $value): FactoryInterface;

    /**
     * Метод возвращает конфигурацию для создания объектов.
     *
     * @return array
     */
    public function getConfig(): array;
}
