<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\result\interfaces;

use Exception;

/**
 * Интерфейс BaseResultObjectInterface для всех результатов в виде массивов.
 */
interface BaseResultObjectInterface
{
    /**
     * Метод задает результат.
     *
     * @param BaseResultInterface $value Новое значение.
     *
     * @return static
     */
    public function setResult(BaseResultInterface $value);

    /**
     * Метод возвращает результат.
     *
     * @throws Exception Если результат не задан.
     *@return BaseResultInterface
     *
     */
    public function getResult(): BaseResultInterface;
}
