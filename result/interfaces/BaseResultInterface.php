<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\result\interfaces;

/**
 * Базовый интерфейс для объекта-результата.
 */
interface BaseResultInterface extends ObjectWithErrorsInterface
{
    /**
     * Метод задает данные результата.
     *
     * @param array $value Новое значение.
     *
     * @return static
     */
    public function setData(array $value): BaseResultInterface;

    /**
     * Метод возвращает данные результата.
     *
     * @return array
     */
    public function getData(): array;

    /**
     * Метод возвращает признак отсутствия ошибок.
     *
     * @return bool
     */
    public function isSuccess(): bool;
}
