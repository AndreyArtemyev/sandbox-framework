<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\result\interfaces;

/**
 * Интерфейс ObjectWithErrorsInterface.
 * Базовый интерфейс для объекта-результата.
 */
interface ObjectWithErrorsInterface
{
    /**
     * Метод задает ошибку.
     *
     * @param string $description Описание ошибки.
     * @param string $name        Название ошибки.
     *
     * @return static
     */
    public function addError(string $description, string $name = 'system'): BaseResultInterface;

    /**
     * Метод возвращает ошибки.
     *
     * @return array
     */
    public function getErrorList(): array;

    /**
     * Метод возвращает признак отсутствия ошибок.
     *
     * @return bool
     */
    public function isSuccess(): bool;
}
