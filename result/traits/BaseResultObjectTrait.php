<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\result\traits;

use Exception;
use SandBox\SandBoxFramework\result\interfaces\BaseResultInterface;

/**
 * Трэит WithDataResult добавляет свойство результата к классу.
 */
trait BaseResultObjectTrait
{
    /**
     * Свойтсво содержит результат.
     *
     * @var BaseResultInterface|null
     */
    protected $result;

    /**
     * Метод задает результат.
     *
     * @param BaseResultInterface $value Новое значение.
     *
     * @return static
     */
    public function setResult(BaseResultInterface $value)
    {
        $this->result = $value;

        return $this;
    }

    /**
     * Метод возвращает результат.
     *
     * @throws Exception Если результат не задан.
     *@return BaseResultInterface
     *
     */
    public function getResult(): BaseResultInterface
    {
        if (null === $this->result) {
            throw new Exception('Объект результата не может быть null.');
        }

        return $this->result;
    }
}
