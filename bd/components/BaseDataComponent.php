<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\components;

use Exception;
use SandBox\SandBoxFramework\base\BaseObject;
use SandBox\SandBoxFramework\bd\interfaces\BaseDataComponentInterface;
use SandBox\SandBoxFramework\bd\interfaces\MysqlConnectionInterfase;
use SandBox\SandBoxFramework\factory\interfaces\FactoryInterface;
use SandBox\SandBoxFramework\factory\traits\ModelsFactoryTrait;


class BaseDataComponent extends BaseObject implements BaseDataComponentInterface
{
    use ModelsFactoryTrait {
        ModelsFactoryTrait::getModelFactory as getModelFactoryFromTrait;
    }

    /**
     * Метод возвращает операцию создания одного экземпляра сущности "Блок голосования".
     *
     * @return MysqlConnectionInterfase
     */
    public function getConnection(): MysqlConnectionInterfase
    {
        return $this->getModelFactory()->getConnection();
    }

    /**
     * Метод возвращает фабрику моделей пакета "Блок голосования".
     *
     * @throws Exception Генерируется если фабрика не имплементирует нужный интерфейс.
     *
     * @return FactoryInterface
     */
    public function getModelFactory(): FactoryInterface
    {
        $modelFactory = $this->getModelFactoryFromTrait();
        if (! $modelFactory instanceof FactoryInterface) {
            throw new Exception('Class ' . get_class($modelFactory) . ' must implement interface ' . FactoryInterface::class);
        }
        return $modelFactory;
    }
}