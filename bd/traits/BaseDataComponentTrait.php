<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\db\traits;

use Exception;
use SandBox\SandBoxFramework\base\Base;
use SandBox\SandBoxFramework\db\interfaces\BaseDataComponentInterface;

/**
 * Трэит WithDatabaseComponent подключает переменную компонента к классу.
 */
trait BaseDataComponentTrait
{
    /**
     * Свойство содержит компонент.
     *
     * @var BaseDataComponentInterface|null
     */
    protected $dbComponent;

    /**
     * Метод возвращает компонент.
     *
     * @throws Exception Если компонент не зарегистирован.
     *@return BaseDataComponentInterface
     *
     */
    protected function getDatabaseComponent(): BaseDataComponentInterface
    {
        if (null === $this->dbComponent) {
            $this->dbComponent = Base::getApplication()->getComponent(BaseDataComponentInterface::COMPONENT_NAME);
        }

        return $this->dbComponent;
    }

    /**
     * Метод задает компонент.
     *
     * @param BaseDataComponentInterface $value Новое значение.
     *
     * @return void
     */
    protected function setDatabaseComponent(BaseDataComponentInterface $value): void
    {
        $this->dbComponent = $value;
    }
}
