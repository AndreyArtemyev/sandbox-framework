<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\factories;


use Exception;
use SandBox\SandBoxFramework\db\interfaces\DataBaseConnectionInterface;
use SandBox\SandBoxFramework\db\interfaces\MysqlConnectionInterfase;
use SandBox\SandBoxFramework\factory\Factory;
use SandBox\SandBoxFramework\factory\interfaces\FactoryInterface;
use SandBox\SandBoxFramework\result\interfaces\BaseResultObjectInterface;

/**
 * Класс IFactory реализует методы фабрики миграций.
 */
class BaseDataFactory extends Factory implements FactoryInterface
{

    public const CONNECTION = 'connection';
    public const RESULT     = 'dataResult';

    /**
     * Метод возвращает объект для соединения с БД.
     *
     * @return MysqlConnectionInterfase
     *
     * @throws Exception Если отсутствует нужный ключ в конфигурации.
     */
    public function getConnection(): MysqlConnectionInterfase
    {
        $object = $this->getInstance(static::CONNECTION);
        if ($object instanceof BaseResultObjectInterface) {
            $object->setResult($this->getResult());
        }

        return $object;
    }

    /**
     * Метод возвращает объект для соединения с БД.
     *
     * @return BaseResultObjectInterface
     *
     * @throws Exception Если отсутствует нужный ключ в конфигурации.
     */
    protected function getResult(): BaseResultObjectInterface
    {
        return $this->getInstance(static::RESULT);
    }
}
