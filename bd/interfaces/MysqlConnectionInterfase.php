<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\interfaces;

use Exception;
use SandBox\SandBoxFramework\result\interfaces\BaseResultInterface;

/**
 * Интерфейс IConnection объявляет методы соединения с БД.
 */
interface MysqlConnectionInterfase extends DataBaseConnectionInterface
{
    /**
     * Метод исполнения прямых запросов к БД.
     *
     * @param string $query Строка для БД.
     *
     * @return BaseResultInterface
     *
     * @throws Exception Если что-то пошло не так с БД подключением.
     */
    public function execute(string $query): BaseResultInterface;

    /**
     * Метод экранирует строку для MySQL.
     *
     * @param string $value Строка для экранизации.
     *
     * @return string
     */
    public function escapeString(string $value): string;

    /**
     * Метод возвращает идентификатор последней созданной записи.
     *
     * @return int|null
     */
    public function getLastInsertId(): ?int;
}
