<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\interfaces;

/**
 * Интерфейс IWithConnection объявляет методы соединения с БД.
 */
interface DataBaseConnectionInterface
{
    /**
     * Метод возвращает соединение с БД.
     *
     * @return MysqlConnectionInterfase
     */
    public function getConnection(): MysqlConnectionInterfase;

    /**
     * Метод задает соединение с БД.
     *
     * @param MysqlConnectionInterfase $value новое значение.
     *
     * @return static
     */
    public function setConnection(MysqlConnectionInterfase $value);
}
