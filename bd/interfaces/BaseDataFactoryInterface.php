<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\interfaces;

/**
 * Интерфейс IFactory объявляет методы фабрики БД.
 */
interface BaseDataFactoryInterface
{
    /**
     *  Метод возвращает объект для соединения с БД.
     *
     * @return MysqlConnectionInterfase
     */
    public function getConnection(): MysqlConnectionInterfase;
}
