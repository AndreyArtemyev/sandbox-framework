<?php

declare(strict_types = 1);

namespace SandBox\SandBoxFramework\bd\interfaces;

/**
 * Интерфейс IFactory объявляет методы компонента БД.
 */
interface BaseDataComponentInterface
{
    public const COMPONENT_NAME = 'db';

    /**
     * Метод возвращает объект для соединения с БД.
     *
     * @return MysqlConnectionInterfase
     */
    public function getConnection(): MysqlConnectionInterfase;
}
